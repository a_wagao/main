#!/usr/bin/env python 
# -*- coding: utf-8 -*-

#顔認識はデータセットに依存する→データセットを作る必要
#多すぎると過学習
#男女がはっきりわかるような写真
#

#Trainerを使ったニューラルネットワークの実装
#コードを簡単にすることができる
#データセット,iterator,ネットワークの準備

from __future__ import print_function
import matplotlib.pyplot as plt
from chainer.datasets import mnist
from chainer.datasets import split_dataset_random
from chainer import iterators
import chainer.links as L
import chainer.functions as F
import random
import chainer
from chainer import optimizers
import numpy as np
from chainer.dataset import concat_examples
from chainer.cuda import to_cpu
from chainer import serializers
from chainer import training
from chainer.training import extensions
from IPython.display import Image

def reset_seed(seed=0):
    random.seed(seed)
    np.random.seed(seed)
reset_seed(0)

train_val, test = mnist.get_mnist()
train, valid = split_dataset_random(train_val, 50000, seed=0)

batchsize = 128

train_iter = iterators.SerialIterator(train, batchsize)
valid_iter = iterators.SerialIterator(valid, batchsize, False, False)
test_iter = iterators.SerialIterator(test, batchsize, False, False)

gpu_id = -1

class MLP(chainer.Chain): #Chainクラスを継承　(モジュール名).でモジュールを呼び出すよ
    def __init__(self, n_mid_units=100, n_out=10):
        super(MLP, self).__init__()
        
        #パラメータを持つ層Linkの登録
        with self.init_scope():
            self.l1 = L.Linear(None, n_mid_units) #L.Linear:全結合層
            self.l2 = L.Linear(n_mid_units, n_mid_units)
            self.l3 = L.Linear(n_mid_units, n_out)

    def __call__(self, x):
        #データを受け取ったときの順伝播計算
        h1 = F.relu(self.l1(x))
        h2 = F.relu(self.l2(h1))
        return self.l3(h2)

gpu_id = -1
net = MLP()

if gpu_id >= 0:
    net.to_gpu(gpu_id)


# Updaterオブジェクトの作成
gpu_id = -1
net = L.Classifier(net) # ネットワークをClassifierで包んで誤差関数の計算などをモデルに含める
optimizer = optimizers.SGD(lr=0.01).setup(net) # 最適化手法の選択
updater = training.StandardUpdater(train_iter, optimizer,  device=gpu_id)

max_epoch = 10
trainer = training.Trainer(updater, (max_epoch, 'epoch'), out='mnist_result') #outで出力ディレクトリを指定

trainer.extend(extensions.LogReport())
trainer.extend(extensions.snapshot(filename='snapshot_epoch-{.updater.epoch}'))
trainer.extend(extensions.Evaluator(valid_iter, net, device=gpu_id), name='val')
trainer.extend(extensions.PrintReport(['epoch', 'main/loss', 'main/accuracy', 'val/main/loss', 'val/main/accuracy', 'l1/W/data/std', 'elapsed_time']))
trainer.extend(extensions.ParameterStatistics(net.predictor.l1, {'std': np.std}))
trainer.extend(extensions.PlotReport(['l1/W/data/std'], x_key='epoch', file_name='std.png'))
trainer.extend(extensions.PlotReport(['main/loss', 'val/main/loss'], x_key='epoch', file_name='loss.png'))
trainer.extend(extensions.PlotReport(['main/accuracy', 'val/main/accuracy'], x_key='epoch', file_name='accuracy.png'))
trainer.extend(extensions.dump_graph('main/loss'))

trainer.run()

Image(filename='mnist_result/loss.png')
Image(filename='mnist_result/accuracy.png')
Image(filename='mnist_result/cg.png')



#テストデータで評価する
test_evaluator = extensions.Evaluator(test_iter, net, device=gpu_id)
results = test_evaluator()
print('Test accuracy:', results['main/accuracy'])

#学習済みデータで推論する
reset_seed(0)

infer_net = MLP()
serializers.load_npz('mnist_result/snapshot_epoch-10', infer_net, path='updater/model:main/predictor/')

if gpu_id >= 0:
    infer_net.to_gpu(gpu_id)

x, t = test[0]
plt.imshow(x.reshape(28, 28), cmap='gray')
plt.show()

x = infer_net.xp.asarray(x[None, ...])
with chainer.using_config('train', False), chainer.using_config('enable_backprop', False):
    y = infer_net(x)
y = to_cpu(y.array)

print('予測ラベル:', y.argmax(axis=1)[0])