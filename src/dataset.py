#!/usr/bin/env/ python
# -*- coding: utf-8 -*-

#chainerチュートリアル
#データ画像の例示から数字の予測

#%matplotlib inline
from __future__ import print_function
import matplotlib.pyplot as plt
from chainer.datasets import mnist
from chainer.datasets import split_dataset_random
from chainer import iterators
import chainer.links as L
import chainer.functions as F
import random
import chainer
from chainer import optimizers
import numpy as np
from chainer.dataset import concat_examples
from chainer.cuda import to_cpu
from chainer import serializers

#データ画像の例示をする
train_val, test = mnist.get_mnist(withlabel=True, ndim=1)
x, t = train_val[0] #0番目の(data, label)を取り出す
plt.imshow(x.reshape(28, 28), cmap='gray')
plt.axis('off')
plt.show()
print('label:', t)


#元データのtrain_val(60000)を50000のtrainデータセット，残りをvalidデータセットに分ける関数
train, valid = split_dataset_random(train_val, 50000, seed=0)
print('Training dataset size:', len(train))
print('Validation dataset size:', len(valid))

#データセットからデータとラベルを取得してそれを束ねてミニバッジにする(Iterator)
batchsize = 128
train_iter = iterators.SerialIterator(train, batchsize)
valid_iter = iterators.SerialIterator(valid, batchsize, repeat=False, shuffle=False)
test_iter = iterators.SerialIterator(test, batchsize, repeat=False, shuffle=False)

#乱数種を固定する(サンプルと同じ結果を保証するため)
def reset_seed(seed=0):
        random.seed(seed)
        np.random.seed(seed)
        '''
        if chainer.cuda.available:
            chainer.cuda.cupy.random.seed(seed)
        '''
reset_seed(0)

#全結合層のみからなる多層パーセプトロンを作るよ
#以下はネットワークを表すコード
class MLP(chainer.Chain): #Chainクラスを継承　(モジュール名).でモジュールを呼び出すよ
    def __init__(self, n_mid_units=100, n_out=10):
        super(MLP, self).__init__()
        
        #パラメータを持つ層Linkの登録
        with self.init_scope():
            self.l1 = L.Linear(None, n_mid_units) #L.Linear:全結合層
            self.l2 = L.Linear(n_mid_units, n_mid_units)
            self.l3 = L.Linear(n_mid_units, n_out)

    def __call__(self, x):
        #データを受け取ったときの順伝播計算
        h1 = F.relu(self.l1(x))
        h2 = F.relu(self.l2(h1))
        return self.l3(h2)

gpu_id = -1 #CPUを使うときは値が-1

net = MLP()

#これ必要？
if gpu_id >= 0:
    net.to_gpu(gpu_id)

optimizer = optimizers.SGD(lr=0.01).setup(net) 
#lr(SGDのコンストラクタ):学習率
#SGDをMomentumSGD,RMSprop,Adamに変えればそれぞれの手法を試せる 


#手書き文字10種類の学習をする
max_epoch = 15

while train_iter.epoch < max_epoch:

    #----学習1のイテレーション----
    train_batch = train_iter.next()
    x, t = concat_examples(train_batch, gpu_id)

    #予測値の計算
    y = net(x)

    #ロスの計算
    loss = F.softmax_cross_entropy(y,t)

    #勾配の計算
    net.cleargrads()
    loss.backward()

    #パラメータの更新
    optimizer.update()
    #----ここまで----

    #1エポック終了(データセットが完全に通過)ごとにValidationデータに対する予測精度を測ってモデルの汎化性能が向上していることを確認
    if train_iter.is_new_epoch: #1エポックが終わったら
        #epochには何番目かの番号
        #ロスの表示
        print('epoch:{:02d} train_loss:{:.04f} '.format(train_iter.epoch, float(to_cpu(loss.data))), end='')
        
        valid_losses = []
        valid_accuracies = []
        while True:
            valid_batch = valid_iter.next()
            x_valid, t_valid = concat_examples(valid_batch, gpu_id)

            #validationデータを順伝播計算
            with chainer.using_config('train', False), chainer.using_config('enable_backprop', False):
                y_valid = net(x_valid)
            
            #ロスを計算
            loss_valid = F.softmax_cross_entropy(y_valid, t_valid)
            valid_losses.append(to_cpu(loss_valid.array))

            #精度を計算
            accuracy = F.accuracy(y_valid, t_valid)
            accuracy.to_cpu()
            valid_accuracies.append(accuracy.array) #valid_accuraciesリストにarrayを追加(appendメソッド)

            if valid_iter.is_new_epoch:
                valid_iter.reset()
                break

        print('val_loss:{:.04f} val_accuracy:{:.04f}'.format(np.mean(valid_losses), np.mean(valid_accuracies)))

#テストデータでの評価
test_accuracies = []
while True:
    test_batch = test_iter.next()
    x_test, t_test = concat_examples(test_batch, gpu_id)
                
    #テストデータを順伝播
    with chainer.using_config('train', False), chainer.using_config('enable_backprop', False):
        y_test = net(x_test)

    #精度を計算
    accuracy = F.accuracy(y_test, t_test)
    accuracy.to_cpu()
    test_accuracies.append(accuracy.array)

    if test_iter.is_new_epoch:
        test_iter.reset()
        break

print('test_accuracy:{:.04f}'.format(np.mean(test_accuracies)))


#学習済みモデルを保存する
serializers.save_npz('my_mnist.model', net)


#保存したモデルを読み込んで推論する
#この場合，学習したネットワークを数字分類したい人に渡して使ってもらう
#pythonファイルと保存したNPZファイルを渡して使う

#準備：渡したネットワーク定義のファイルからネットワークのクラスが読み込まれていることを確認
infer_net = MLP() #同じネットワークのオブジェクトを作る.このときMLPはネットワークのクラス
serializers.load_npz('my_mnist.model', infer_net) #そのオブジェクトに保存済みパラメータをロード

#テストデータから1つ目の画像をとってきて分類を行う
gpu_id = -1
if gpu_id >= 0:
    infer_net.to_gpu(gpu_id)

#1つ目のテストデータを取り出す
x, t = test[0] #tは使わない
plt.imshow(x.reshape(28,28), cmap='gray')
plt.show()

#ミニバッジの形式にする
print('元の形:', x.shape, end=' -> ')
x = x[None, ...]
print('ミニバッジの形にしたあと:', x.shape)

#ネットワークと同じデバイス上にデータを送る
x = infer_net.xp.asarray(x)
#モデルのforward関数に渡す
with chainer.using_config('train', False), chainer.using_config('enable_backprop', False):
    y = infer_net(x)
y = y.array #Variable形式で出てくるので中身を取り出す
y = to_cpu(y) #結果をCPUに送る
pred_label = y.argmax(axis=1)
print('ネットワークの予測:', pred_label[0])